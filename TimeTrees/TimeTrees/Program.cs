﻿using System;
using System.IO;
using System.Globalization;
using Newtonsoft.Json;

class Test
{
    public struct TimelineEvent
    {
        public string Data;
        public string Event;
    }

    public struct Person
    {
        public int Id;
        public string Name;
        public string Birth;
        public string Death;
    }

    static DateTime StringToDateTime(string local)
    {
        DateTime Date;
        if (!DateTime.TryParseExact(local, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out Date))
            if (!DateTime.TryParseExact(local, "yyyy-mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out Date))
                DateTime.TryParseExact(local, "yyyy-mm-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out Date);
        return Date;
    }

    static string FindFile(string text)
    {
        string jsontext = text + ".json";
        string csvtext = text + ".csv";
        string json = Path.Combine(Environment.CurrentDirectory, jsontext);
        string csv = Path.Combine(Environment.CurrentDirectory, csvtext);

        if (File.Exists(json))
            return json;
        if (File.Exists(csv))
            return csv;
        Console.WriteLine("NOT FOUND");
        return "";
    }

    static Person[] JsonFilePeople(string path)
    {
        string file = File.ReadAllText(path);
        return JsonConvert.DeserializeObject<Person[]>(file);
    }

    static TimelineEvent[] JsonFileEvent(string path)
    {
        string file = File.ReadAllText(path);
        return JsonConvert.DeserializeObject<TimelineEvent[]>(file);
    }

    static Person[] CsvFilePeople(string path)
    {
        string[] readpeople = File.ReadAllLines(path);
        Person[] people = new Person[readpeople.Length];

        for (int i = 0; i < readpeople.Length; i++)
        {
            string[] Localpeople;
            Localpeople = readpeople[i].Split(';');
            people[i].Name = Localpeople[1];
            people[i].Id = Int32.Parse(Localpeople[0]);
            people[i].Birth = Localpeople[2];
            
            if (Localpeople.Length == 4)
                people[i].Death = Localpeople[3];
        }

        return people;
    }

    static TimelineEvent[] CsvFileEvent(string path)
    {
        string[] readtimeline = File.ReadAllLines(path);
        TimelineEvent[] timeline = new TimelineEvent[readtimeline.Length];

        for (int i = 0; i < readtimeline.Length; i++)
        {
            string[] Localtimeline;
            Localtimeline = readtimeline[i].Split(';');
            timeline[i].Data = Localtimeline[0];
            timeline[i].Event = Localtimeline[1];
        }

        return timeline;
    }

    static void HighYear(Person[] people)
    {
        foreach (Person person in people) {
            DateTime dateBirth = StringToDateTime(person.Birth);
            DateTime dateDeath = StringToDateTime(person.Death);
            if (dateBirth.Year % 4 == 0 && DateTime.Now.Year - dateBirth.Year < 20)
                Console.WriteLine(person.Name);
        }
    }

    static void DateDifference(TimelineEvent[] events)
    {
        DateTime minDate = DateTime.MaxValue;
        DateTime maxDate = DateTime.MinValue;
        DateTime date;
        foreach (TimelineEvent history in events)
        {
            date = StringToDateTime(history.Data);
            if (maxDate < date)
                maxDate = date;
            if (minDate > date)
                minDate = date;
        }

        Console.WriteLine("{0}, {1}, {2}", maxDate.Year - minDate.Year, maxDate.Month - minDate.Month, maxDate.Day - minDate.Day);

    }

    static Person[] ChooseExtensionPeople(string peopleFile)
    {
        Person[] people = new Person[10];
        string peopleExtension = Path.GetExtension(peopleFile);
        switch (peopleExtension)
        {
            case ".json":
                people = JsonFilePeople(peopleFile);
                break;
            case ".csv":
                people = CsvFilePeople(peopleFile);
                break;
        }
        return people;
    }

    static TimelineEvent[] ChooseExtensionEvents(string timelineFile)
    {
        string eventExtension = Path.GetExtension(timelineFile);
        switch (eventExtension)
        {
            case ".json":
                return JsonFileEvent(timelineFile);
                break;
            case ".csv":
                return CsvFileEvent(timelineFile);
                break;
        }
        return null;
    }
    public static void Main()
    {
        string timelineFile = FindFile("timeline");
        string peopleFile = FindFile("people");

        TimelineEvent[] timeline = ChooseExtensionEvents(timelineFile); // Главный костыль
        Person[] people = ChooseExtensionPeople(peopleFile); // Главный костыль

        HighYear(people);
        DateDifference(timeline);

    }

}